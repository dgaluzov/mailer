package com.getjavajob.training.web1904.galuzovd.common;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.Objects;

@Document(collection = "Notice")
public class Notice {

    @Id
    private ObjectId id;
    @Field(value = "email")
    private String email;
    @Field(value = "messageText")
    private String messageText;

    public Notice(String email, String messageText) {
        this.email = email;
        this.messageText = messageText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Notice notice = (Notice) o;
        return Objects.equals(id, notice.id) &&
                Objects.equals(email, notice.email) &&
                Objects.equals(messageText, notice.messageText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, messageText);
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
}
