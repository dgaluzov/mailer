package com.getjavajob.training.web1904.galuzovd.dao;

import com.getjavajob.training.web1904.galuzovd.common.Notice;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NoticeDao extends MongoRepository<Notice, Long> {
}
