package com.getjavajob.training.web1904.galuzovd;

import java.util.Objects;

public class JmsMessage {
    private String email;
    private String messageText;

    public JmsMessage(String email, String messageText) {
        this.email = email;
        this.messageText = messageText;
    }

    public JmsMessage() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        JmsMessage jmsMessage = (JmsMessage) o;
        return Objects.equals(email, jmsMessage.email) &&
                Objects.equals(messageText, jmsMessage.messageText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, messageText);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
}
