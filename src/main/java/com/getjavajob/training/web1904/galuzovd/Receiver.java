package com.getjavajob.training.web1904.galuzovd;

import com.getjavajob.training.web1904.galuzovd.common.Notice;
import com.getjavajob.training.web1904.galuzovd.dao.NoticeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;

import static com.getjavajob.training.web1904.galuzovd.ActiveMQConfiguration.ORDER_QUEUE;

@Component
public class Receiver {
    private final NoticeDao noticeDao;
    private final JavaMailSender javaMailSender;

    @Autowired
    public Receiver(NoticeDao noticeDao, JavaMailSender javaMailSender) {
        this.noticeDao = noticeDao;
        this.javaMailSender = javaMailSender;
    }

    @JmsListener(destination = ORDER_QUEUE)
    public void receiveMessage(JmsMessage jmsMessage) {
        noticeDao.save(new Notice(jmsMessage.getEmail(), jmsMessage.getMessageText()));
        sendMail(jmsMessage);
    }

    private void sendMail(JmsMessage jmsMessage) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(jmsMessage.getEmail());
        simpleMailMessage.setSubject("Test Notice");
        simpleMailMessage.setText(jmsMessage.getMessageText());
        javaMailSender.send(simpleMailMessage);
    }
}